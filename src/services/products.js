import axios from "axios";

export const createProduct = async (product) => {
	return axios.post("http://localhost:4000/product", product);
};
export const deleteProducts = async (products) => {
	return axios.post("http://localhost:4000/product/delete", {products : products});
};
export const updateProduct = async (product) => {
	return axios.put(`http://localhost:4000/product/${product.id}`, product);
};

export const getProducts = async () => {
	return axios.get("http://localhost:4000/product");
};
