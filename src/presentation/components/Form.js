import React from "react";
import {
	Grid,
	makeStyles,
	Card,
	CardContent,
	MenuItem,
	InputLabel,
	Select,
	Button,
	CardHeader,
	FormControl,
	CardActions
} from "@material-ui/core";
import { Formik, Form, Field } from "formik";
import { TextField } from "formik-material-ui";

const useStyle = makeStyles((theme) => ({
	padding: {
		padding: theme.spacing(3)
	},
	button: {
		margin: theme.spacing(1)
	}
}));



const ProductoForm = (props) => {
	const { initialState, fields, title, validations, createProduct } = props;
	const classes = useStyle();

	const onSubmit = (values) => {
		createProduct(values);
	};

	return (
		<Grid container justify="center" spacing={1}>
			<Grid item md={12}>
				<Card className={classes.padding}>
					<CardHeader title={title}></CardHeader>
					<Formik
						initialValues={initialState}
						validationSchema={validations}
						onSubmit={onSubmit}
					>
						{({ dirty, isValid, values, handleChange, handleBlur }) => {
							return (
								<Form>
									<CardContent>
										<Grid item container spacing={1} justify="center">
											{fields.map((field, key) => {
												if(field.type == "Dropdown"){
													return (
													<Grid item xs={12}>
												<FormControl fullWidth variant="outlined">
													<InputLabel id="demo-simple-select-outlined-label">
														Categoria
													</InputLabel>
													<Select
														labelId="demo-simple-select-outlined-label"
														id="demo-simple-select-outlined"
														label="Productos"
														onChange={handleChange}
														onBlur={handleBlur}
														value={values[field.field]}
														name={field.field}
													>
														{field.data.map((item) => (
															<MenuItem key={item.id} value={item}>
																{item.category}
															</MenuItem>
														))}
													</Select>
												</FormControl>
											</Grid>
											);
											}else{
												return (
													<Grid key={key} item xs={12}>
														<Field
															label={field.label}
															variant="outlined"
															fullWidth
															name={field.field}
															value={values[field.field]}
															component={TextField}
														/>
													</Grid>
												);
											}
											})}
										</Grid>
									</CardContent>
									<CardActions>
										<Button
											disabled={!dirty || !isValid}
											variant="contained"
											color="secondary"
											type="Submit"
											className={classes.button}
										>
											Crear
										</Button>
									</CardActions>
								</Form>
							);
						}}
					</Formik>
				</Card>
			</Grid>
		</Grid>
	);
};

export default ProductoForm;
