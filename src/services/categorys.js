import axios from "axios";

export const createCategory = async (category) => {
	return axios.post("http://localhost:4000/category", category);
};
export const deleteCategory = async (products) => {
	return axios.post("http://localhost:8000/api/products/delete", {products : products});
};
export const updateCategory = async (product) => {
	return axios.put(`http://localhost:8000/api/products/${product.id}`, product);
};

export const getCategorys = async () => {
	return axios.get("http://localhost:4000/category");
};
