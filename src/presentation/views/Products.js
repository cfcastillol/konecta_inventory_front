import React, { useState, useEffect } from "react";
import DataTable from "../components/Table";
import Modal from "../components/Modal";
import Button from "@material-ui/core/Button";
import ProductoForm from "../components/Form";
import * as Yup from "yup";
import Swal from "sweetalert2";
import { createProduct, updateProduct,deleteProducts } from "../../services/products";
import { createCategory, getCategorys } from "../../services/categorys";

const Products = (props) => {
	const { products,updateData } = props;
	const [modal, setModal] = useState(false);
	const [category, setCategory] = useState(false);
	
	const [categorys, setCategorys] = React.useState([]);
	const [productsSelected, setProductsSelected] = useState(false);

	const getData = async () => {
		let categorys = await getCategorys();
		setCategorys(categorys.data);
	};

	useEffect(() => {
		getData();
	}, []);

	const handleClose = () => {
		setModal(false);
	};

	const initialStateProduct = {
		id: "",
		name: "",
		reference: "",
		price: "",
		category_id: "",
		stock: "",
		created_at: "",
		active: ""
	};

	const initialStateCategory = {
		category: "",
	};

	const fields = [
		{
			field: "name",
			label: "Nombre Producto",
			type: "TextField"
		},
		{
			field: "reference",
			label: "Referencia",
			type: "TextField"
		},
		{
			field: "price",
			label: "Precio",
			type: "TextField"
		},	
		{
			field: "category_id",
			label: "Categoria Producto",
			type: "Dropdown",
			data: categorys,
		},
		{
			field: "stock",
			label: "Stock",
			type: "TextField"
		}
	];

	const fieldsCategory = [
		{
			field: "category",
			label: "Nombre ",
			type: "TextField"
		},
		
	];

	const columns = [
		{ field: "id", headerName: "ID", width: 90 },
		{
			field: "name",
			headerName: "Nombre",
			width: 150,
			editable: true
		},
		{
			field: "reference",
			headerName: "Referencia",
			width: 150,
			editable: true
		},
		{
			field: "price",
			headerName: "Precio",
			type: "number",
			width: 150,
			editable: true
		},		
		{
			field: "category",
			headerName: "Categoria",
			width: 150,
		},
		{
			field: "stock",
			headerName: "Stock",
			type: "number",
			width: 150,
			editable: true
		},
		{
			field: "create_at",
			headerName: "Creado",
			width: 200
		}
	];

	const validations = Yup.object().shape({
		name: Yup.string().required("Required"),
		reference: Yup.string().required("Required"),
		price: Yup.number().positive().required("Required"),
		//active: Yup.number().positive().integer().required("Required"),
		 category_id: Yup.object().shape({
		 	category: Yup.string().required("Required"),
		 	id: Yup.number().positive().integer().required("Required")
		 }),
		stock: Yup.number().positive().integer().required("Required")
	});

	const validationsCategory = Yup.object().shape({
		category: Yup.string().required("Required"),
	});

	const deleteProd = () => {
		Swal.fire({
			title: "¿Desea Eliminar los productos seleccionados?",
			showCancelButton: true,
			confirmButtonText: "Eliminar"
		}).then(async (result) => {
			/* Read more about isConfirmed, isDenied below */
			if (result.isConfirmed) {
				let res = await deleteProducts(productsSelected)
				if(res.status == 200){
					Swal.fire("Eliminados!", "", "success");
					updateData()
				}
			}
		});
	};

	const createProd = async (product) => {

		let customProd = {...product, category_id:product.category_id.id}

		let res = await createProduct(customProd);
		if (res.status == 200) {
			Swal.fire("Producto añadido!", "", "success");
			setModal(false)
			updateData()
		} else {
			Swal.fire("Error Creando Producto!", "", "error");
		}
	};

	const createCat = async (category) => {
		let res = await createCategory(category);
		if (res.status == 200) {
			Swal.fire("Categoria añadido!", "", "success");
			setModal(false)
			updateData()
		} else {
			Swal.fire("Error Creando Categoria!", "", "error");
		}
	};

	const updateProd = async (product) => {
		console.log(product);
		let res = await updateProduct({
			...product.row,
			[product.field]: product.value
		});
		if (res.status == 200) {
			Swal.fire("Producto Modificado!", "", "success");
			updateData()
		} else {
			Swal.fire("Error Modificando Producto!", "", "error");
		}
	};
	return (
		<>
			<Modal open={modal} handleClose={handleClose}>
				<>
					<ProductoForm
						initialState={category? initialStateCategory : initialStateProduct}
						fields={category? fieldsCategory : fields}
						title={category? "Crear Categoria" : "Crear Producto"}
						validations={category? validationsCategory : validations}
						createProduct={category? createCat : createProd}
					/>
				</>
			</Modal>
			<div>
				<Button
					style={{ margin: "1em" }}
					variant="contained"
					color="secondary"
					size={"medium"}
					onClick={() => {
						setModal(true)
						setCategory(false)
					}}
				>
					Crear Producto
				</Button>

				<Button
					style={{ margin: "1em" }}
					variant="contained"
					color="secondary"
					size={"medium"}
					onClick={() => {
						setCategory(true)
						setModal(true)
					}}
				>
					Crear Categoria
				</Button>

				{productsSelected.length > 0 && (
					<Button
						style={{ margin: "1em" }}
						variant="contained"
						color="secondary"
						size={"medium"}
						onClick={deleteProd}
					>
						Eliminar Productos
					</Button>
				)}
			</div>
			<DataTable
				rows={products}
				columns={columns}
				productsSelected={setProductsSelected}
				updateProduct={updateProd}
			/>
		</>
	);
};

export default Products;
